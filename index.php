<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laracasts PlayList Downloader</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>

	<div class="container">
		<h3 class="text text-danger">Laracasts PlayList Downloader</h3>
		<div class="panel panel-info">
			<div class="panel-heading">
			<form method="get">				
				<div class="col-md-10">
					<input type="text" name="url" placeholder="Enter URL" value="<?= @$_GET['url'] ?>" class="form-control">
				</div>
				<div class="col-md-2">
					<button class="btn btn-primary">Get</button>
				</div>
				<div class="clearfix"></div>
			</form>
			</div>
			<div class="panel-body">
				<table class="table table-bordered">
					<?php
						if(isset($_GET['url'])){
							$url = $_GET['url'];


							$content = file_get_contents($url);

							$content = str_replace('class=""','',$content);

							preg_match_all("/<td[^>]*class=\"episode-title\">(.*?)<\/td>/ms", $content, $matches);


							$series_links = []; 

							foreach($matches[0] as $link){

								// get the title
								preg_match("/<span class=\"episode-title__body\">(.*)<\/span>/ims", $link, $matches2);
								
								// get the link

								preg_match_all("/<a href=\"(.*)\"$/ms", $link, $matches3);


								$series_links[] = [ 
									'title' => $matches2[0],
									'link' => $matches3[1][0],
									'free' => (preg_match("/free/ims", $link, $match) > 0) ? '<span class="label label-primary">Free</span>' : '<span class="label label-danger">Premium</span>', // check if lession is free
								];

							}




							$int = 0;
							foreach($series_links as $link){
								$title = str_replace('FREE','',$link['title']);
								$free = $link['free'];
								$url = 'http://laracasts.com'.$link['link'];
								$content = file_get_contents($url);
								$content = preg_replace("/(\/[^>]*>)([^<]*)(<)/","\\1\\3",$content);
								preg_match_all("/\/downloads\/(.*?)\/?type=(.*?)'/ms", $content, $matches);	
								$link = rtrim($matches[0][0],"'");
								$link = 'http://laracasts.com'.$link;	
								echo  "<tr>
											<td>".++$int."</td>
											<td>".$title."</td>
											<td>".$free."</td>
											<td><a href='{$link}' class='btn btn-danger btn-xs' target='_blank'>Download</a></td>
										</tr>";
								ob_flush();
								flush();
							}
							echo "<tr class='alert alert-danger text-center'><td colspan='4'>End Of The List </td></tr>";
							}
							?>

				</table>
			</div>
		</div>
	</div>

	
</body>
</html>